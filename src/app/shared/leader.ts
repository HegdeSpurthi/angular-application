// Assignment 2: Task 1
export class Leader {
    id: number;
    name: string;
    image: string;
    designation: string;
    abbr: string;
    featured: boolean;
    description: string;
}
