import { Injectable } from '@angular/core';
import { Dish } from '../shared/dish';
import { DISHES } from '../shared/dishes';
import { Observable, of } from 'rxjs';
import { delay, map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { baseURL } from '../shared/baseurl';
import { ProcessHTTPMsgService } from './process-httpmsg.service';
import { Restangular } from 'ngx-restangular';

@Injectable({
  providedIn: 'root'
})
export class DishService {

  constructor(private http: HttpClient,
    private processHTTPMsgservice: ProcessHTTPMsgService,
    private restangular: Restangular) { }

  getDishes(): Observable<Dish[]> {
    // return of(DISHES).pipe(delay(2000));
    // return this.http.get<Dish[]>(baseURL + 'dishes').pipe(catchError(this.processHTTPMsgservice.handleError));
    return this.restangular.all('dishes').getList();
  }

  getDish(id: number): Observable<Dish> {
    // return of(DISHES.filter((dish) => (dish.id === id))[0]).pipe(delay(2000));
    // return this.http.get<Dish>(baseURL + 'dishes/' + id).pipe(catchError(this.processHTTPMsgservice.handleError));
    return this.restangular.one('dishes', id).get();
  }

  getFeaturedDish(): Observable<Dish> {
    // return of(DISHES.filter(dish => (dish.featured))[0]).pipe(delay(2000));
    // return this.http.get<Dish>(baseURL + 'dishes?featured=true').pipe(map(dish => dish[0]))
    // .pipe(catchError(this.processHTTPMsgservice.handleError));
    return this.restangular.all('dishes').getList({featured: true}).pipe(map(dish => dish[0]));
  }

  getDishIds(): Observable<number[] | any> {
    // return of(DISHES.map(dish => dish.id ));
    // return this.getDishes().pipe(map(dishes => dishes.map(dish => dish.id)))
    // .pipe(catchError(this.processHTTPMsgservice.handleError));
    return this.getDishes()
    .pipe(map(dishes => dishes.map(dish => dish.id)),
      catchError(error => error ));
  }
}
