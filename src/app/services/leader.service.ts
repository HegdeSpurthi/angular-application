import { Injectable } from '@angular/core';
// import { LEADERS } from '../shared/leaders';
import { Leader } from '../shared/leader';
import { Observable, of } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { Restangular } from 'ngx-restangular';
// Assignment 2: Task 1

@Injectable({
  providedIn: 'root'
})
export class LeaderService {

  constructor(private restangular: Restangular) { }
// Assignment 2: Task 1
  getLeaders(): Observable<Leader[]> {
    // return of(LEADERS).pipe(delay(2000));
    return this.restangular.all('leaders').getList();
  }

  // Assignment 2: Task 3
  getFeaturedLeader(): Observable<Leader> {
    // return of(LEADERS.filter(leader => (leader.featured))[0]).pipe(delay(2000));
    return this.restangular.all('leaders').getList({featured: true}).pipe(map(dish => dish[0]));
  }

  getLeader(id: number): Observable<Leader> {
    // return of(LEADERS.filter((leader) => (leader.id === id))[0]).pipe(delay(2000));
    return this.restangular.one('leaders', id).get();
  }
}
