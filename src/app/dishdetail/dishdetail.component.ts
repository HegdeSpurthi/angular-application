import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';
import { Location } from '@angular/common';
import { Params, ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Comment } from '../shared/comment';
// import { trigger, state, style, transition, animate } from '@angular/animations';
import { visibility, expand } from '../animations/app.animations';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  animations: [ visibility(), expand() ]
})

export class DishdetailComponent implements OnInit {
    // @Input()
    @ViewChild('cform') feedbackFormDirective;
    dish: Dish;
    dishIds: number[];
    prev: number;
    next: number;
    commentForm: FormGroup;
    comment: Comment;
    errMess: string;
    dishcopy = null;
    visibility = 'shown';

    formErrors = {
      'comment': '',
      'author': ''
    };

    validationMessages = {
      'comment': {
        'required': 'Comment is required.'
      },
      'author': {
        'required':  'Author is required.',
        'minlength': 'Author must be at least 2 characters long.'
      }
    };

  constructor(private dishService: DishService,
      private route: ActivatedRoute,
      private location: Location,
      private fb: FormBuilder,
      @Inject('BaseURL') private BaseURL) {
        this.createCommentForm();
       }

  ngOnInit() {
    // const id = +this.route.snapshot.params['id'];
    // this.dishService.getDish(id).subscribe(dish => this.dish = dish);
    this.dishService.getDishIds().subscribe(dishIds => this.dishIds = dishIds);
    this.route.params.pipe(
    switchMap((params: Params) => { this.visibility = 'hidden'; return this.dishService.getDish(+params['id']); }))
    .subscribe(dish => { this.dish = dish; this.dishcopy = dish; this.setPrevNext(dish.id); this.visibility = 'shown'; },
        errmess => { this.dish = null; this.errMess = <any>errmess; });
  }

  createCommentForm(): void {
    this.commentForm = this.fb.group({
      comment: ['', Validators.required ],
      rating: 5,
      author: ['', [Validators.required, Validators.minLength(2)] ]
    });
    this.commentForm.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {
    if (!this.commentForm) {return; }
    const form = this.commentForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }

  goBack(): void {
    this.location.back();
  }

  setPrevNext(dishId: number) {
    const index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];
  }

  onCommentSubmit(): void {
    this.comment = this.commentForm.value;
    this.comment.date = new Date().toISOString();
    // console.log(this.comment);
    // this.dish.comments.push(this.comment);
    this.dishcopy.comments.push(this.comment);
    this.dishcopy.save()
      .subscribe(dish => { this.dish = dish; console.log(this.dish); });
    // this.dishcopy.comments.push(this.comment);
    // this.dishcopy.save().subscribe(dish =>  { this.dish = dish; console.log(this.dish); });
    this.commentForm.reset({
      comment: '',
      rating: 5,
      author: ''
    });
    this.feedbackFormDirective.reset();
  }

}
